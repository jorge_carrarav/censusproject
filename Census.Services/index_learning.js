
const Joi = require('joi');
const express = require('express');
const app = express();
const testJson = require('./Data/test.json');
app.use(express.json());

const courses = [
    { id: 1, name: 'Math'},  
    { id: 2, name: 'English'},  
    { id: 3, name: 'Programming'}
];
//This is needed
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  
app.get('/', (req, res) => {
   res.send("Service running");
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.')
    res.send(course)
});

app.post('/api/courses', (req, res) => {
    const schema = {
        name: Joi.string().min(3).required()
    };
    const result = Joi.validate(req.body, schema);
    console.log(result);

    const { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message)

    const course = {
        id: courses.length + 1,
        name: req.body.name
    };
    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');


    const { error } = validateCourse(req.body);
    if (error) return res.status(400).send(error.details[0].message)

    course.name = req.body.name;
    res.send(course)

});


app.delete('/api/courses/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if (!course) return res.status(404).send('The course with the given ID was not found.');

    const index = courses.indexOf(course);
    courses.splice(index,1);
    
    res.send(courses)
});


function validateCourse(couse){
    const schema = {
        name: Joi.string().min(3).required()
    };

    return Joi.validate(couse, schema);
}

//PORT = enviorment variable
const port = process.env.PORT | 3200;
app.listen(port, () => {console.log('Server started on port ' + port + ' ...');});


/*app.post()
app.put()
app.delete()
*/