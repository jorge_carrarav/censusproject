
const Joi = require('joi');
const express = require('express');
const app = express();
app.use(express.json());
const data_testJson = require('./Data/test.json');
const data_industry = require('./Data/industry_nosuperior.json');
const mapRegions = require('./Data/mapRegions.json');
const mapCommunes_veryLight = require('./Data/mapCommunes_veryLight.json');
const mapCommunes_light = require('./Data/mapCommunes_light.json');
const mapCommunes_heavy = require('./Data/mapCommunes_heavy.json');
const topojson = require("topojson-client")

var publicDir = require('path').join(__dirname,'/Public');
app.use(express.static(publicDir));

//This is needed
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// --- Service running
app.get('/', (req, res) => {
   res.send("Service running");
});

// --- Service running
app.get('/api/academicLevel_industry', (req, res) => {

    var industryData = data_industry.widgets.industry_real;
    var links_arr = [];
    for (i = 0; i < industryData.length; i++) {
        var source = industryData[i]._id.level;
        var target = industryData[i]._id.industry;
        var value = industryData[i].count

        
        if(source == 0) source = 0;
        if(source == 11) source = 1;
        if(source == 12) source = 2;
        if(source == 13) source = 3;
        if(source == 14) source = 4;

        if(target == "A") target = 5;
        if(target == "B") target = 6;
        if(target == "C") target = 7;
        if(target == "D") target = 8;
        if(target == "E") target = 9;
        if(target == "F") target = 10;
        if(target == "G") target = 11;
        if(target == "H") target = 12;
        if(target == "I") target = 13;
        if(target == "J") target = 14;
        if(target == "K") target = 15;
        if(target == "L") target = 16;
        if(target == "M") target = 17;
        if(target == "N") target = 18;
        if(target == "O") target = 19;
        if(target == "P") target = 20;
        if(target == "Q") target = 21;
        if(target == "R") target = 22;
        if(target == "S") target = 23;
        if(target == "T") target = 24;
        if(target == "U") target = 25;
        if(target == "Z") target = 26;

        links_arr.push(
            {
                "source": source,
                "target": target,
                "value": value
            }
        )
        
    }
    console.log(links_arr)
    
    data_sankey = {
        "nodes":[
            {"node":0,"name":"Sin estudios superiores"},
            {"node":1,"name":"Técnico"},
            {"node":2,"name":"Profesional"},
            {"node":3,"name":"Magister"},
            {"node":4,"name":"Doctorado"},
    
            {"node":5,"name":"A"},
            {"node":6,"name":"B"},
            {"node":7,"name":"C"},
            {"node":8,"name":"D"},
            {"node":9,"name":"E"},
            {"node":10,"name":"F"},
            {"node":11,"name":"G"},
            {"node":12,"name":"H"},
            {"node":13,"name":"I"},
            {"node":14,"name":"J"},
            {"node":15,"name":"K"},
            {"node":16,"name":"L"},
            {"node":17,"name":"M"},
            {"node":18,"name":"N"},
            {"node":19,"name":"O"},
            {"node":20,"name":"P"},
            {"node":21,"name":"Q"},
            {"node":22,"name":"R"},
            {"node":23,"name":"S"},
            {"node":24,"name":"T"},
            {"node":25,"name":"U"},
            {"node":26,"name":"Z"}    
            ],
        "links": links_arr
        };

    res.send(data_sankey);
 });


// --- Test Data
app.get('/api/testJson', (req, res) => {
    res.send(data_testJson);
});

// --- Regions
app.get('/api/map/:object', (req, res) => {

    if(req.params.object === 'regions'){
        var mapJson = topojson.feature(mapRegions, mapRegions.objects.places).features;
        res.send(mapJson);
    }
    else{
        var mapJson;
        var mapSelection;
        //Santiago uses heavy map since it needs more details
        if(req.params.object === "Región Metropolitana de Santiago")
            mapSelection = mapCommunes_heavy
        else
            mapSelection = mapCommunes_veryLight


        mapJson = topojson.feature(mapSelection, mapSelection.objects.comunas_de_chile).features;
        mapJson = filterMap(mapJson, req.params.object)
        res.send(mapJson);
    }
});



function filterMap(mapJson, region){
        
    for (i = 0; i < mapJson.length; i++) {
        if(mapJson[i].properties.nom_reg !== region 
            || mapJson[i].properties.nom_com === "Isla de Pascua"
            || mapJson[i].properties.nom_com === "Juan Fernández"){
            mapJson.splice(i,1);
            i = i - 1;
        }
    }

    return mapJson;
}

//PORT = enviorment variable
const port = process.env.PORT | 3200;
app.listen(port, () => {console.log('Server started on port ' + port + ' ...');});