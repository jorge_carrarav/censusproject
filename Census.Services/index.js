
const Joi = require('joi');
const express = require('express');
const app = express();
app.use(express.json());

/* MONGO */
const MongoClient = require('mongodb').MongoClient;
const mongoServerURL = 'mongodb://localhost:27017';
const assert = require('assert');
const client = new MongoClient(mongoServerURL, { useNewUrlParser: true });
const dbName = 'censo';
const collectionName = "Personas_light_13";


/* TOPOJSON FILES */
const topojson = require("topojson-client")
const topojson_regions = require('./Data/topojson_regions.json');
const topojson_communes_veryLight = require('./Data/topojson_communes_veryLight.json');
const topojson_communes_light = require('./Data/topojson_communes_light.json');
const topojson_communes_heavy = require('./Data/topojson_communes_heavy.json');

/* DATA */
const data_gender = require('./Data/data_gender.json');
const data_agerange = require('./Data/data_agerange.json');
const data_foreigners = require('./Data/data_foreigners.json');
const data_ethnics = require('./Data/data_ethnics.json');
const data_academicLevel_adults = require('./Data/data_academicLevel_adults.json');
const data_academicLevelIndustry = require('./Data/data_academicLevelIndustry.json');
const data_communes = require('./Data/data_communes.json');
const data_regions = require('./Data/data_regions.json');




var publicDir = require('path').join(__dirname,'/Public');
app.use(express.static(publicDir));

//This is needed to run the APIs
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// --- Service running
app.get('/', (req, res) => {
   res.send("Service running");
});



// ========================================================== GENDER API
app.post('/api/data/gender', (req, res) => {

    if(isEmpty(req.body))
        res.send(data_gender);
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        client.connect(function(err) {
            
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);

            collection.aggregate([
                {
                    $match: match
                },
                {
                    $group: {
                        _id: "$P08",
                        count: {$sum: 1}
                    }    
                },
                {
                    $project: {
                        _id:0,
                        gender: { $cond: [{$eq:["$_id",1]}, "men", "women"] },
                        count: 1
                    }    
                }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }
});

// ========================================================== AGE RANGE API
app.post('/api/data/agerange', (req, res) => {
    if(isEmpty(req.body))
        res.send(data_agerange);    
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },
                { 
                    $project: {    
                      "range": {
                         $concat: [
                            { $cond: [{$and:[ {$gte:["$P09",0 ]}, {$lte:["$P09", 10]}]}, "0 - 10", ""] },
                            { $cond: [{$and:[ {$gte:["$P09",11]}, {$lte:["$P09", 20]}]}, "11 - 20", ""] },
                            { $cond: [{$and:[ {$gte:["$P09",21]}, {$lte:["$P09", 30]}]}, "21 - 30", ""]},
                            { $cond: [{$and:[ {$gte:["$P09",31]}, {$lte:["$P09", 40]}]}, "31 - 40", ""]},
                            { $cond: [{$and:[ {$gte:["$P09",41]}, {$lte:["$P09", 50]}]}, "41 - 50", ""]},
                            { $cond: [{$and:[ {$gte:["$P09",51]}, {$lte:["$P09", 60]}]}, "51 - 60", ""]},
                            { $cond: [{$and:[ {$gte:["$P09",61]}, {$lte:["$P09", 70]}]}, "61 - 70", ""]},
                            { $cond: [{$gte:["$P09",71]}, "71+", ""]}
                         ]
                      }  
                    }    
                  },
                  {
                    $group: { 
                      "_id" : "$range", 
                      count: { $sum: 1 } 
                    }
                  },
                  {
                    $sort: { 
                      _id: 1
                    }
                  },
                  {
                    $project: {
                      _id: 0,
                      range: "$_id",
                      count: 1
                    }
                  }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }

});

// ========================================================== FOREIGNERS API
app.post('/api/data/foreigners', (req, res) => {
    if(isEmpty(req.body))
        res.send(data_foreigners);
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);

        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },
                {
                    $match: {    
                        "P10": {$nin: [4,98,99]}, //Vive habitualmente en esta comuna. 4 = Otro pais (o sea turista)
                        "P12": {$nin: [1,2,98,99]}, //1,2 = comuna de nacimiento
                        "P12PAIS": {$ne: 997}
                    }    
                },
                {
                    $project:{
                        "country": {
                            $cond: { if: { $eq: ["$P12", 8] }, then: "$P12PAIS", else: "$P12" }
                        }
                    }
                },
                {
                    $group:{
                        "_id": "$country",
                        "count": {$sum: 1}
                    }
                },
                {
                    $sort:{
                        "count": -1
                    }
                },
                {
                    $limit: 6
                },
                {
                    $project:{
                        _id: 0,
                        "country": "$_id",
                        count: 1
                    }
                }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }
});

// ========================================================== ETHNICS API
app.post('/api/data/ethnics', (req, res) => {
    if(isEmpty(req.body))
        res.send(data_ethnics);    
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },
                {
                    $match: {
                        P16A: {$nin: [98, 99]}, //Pueblo indígena u originario listado
                        P16A_OTRO: {$ne: 97}
                    }
                },
                {
                    $project: {
                        "P16A": 1,
                        "P16A_OTRO": {$multiply: ["$P16A_OTRO", 1000]}
                    }
                },
                {
                    $project: {
                        "ethnic": {$cond: { if: { $eq: ["$P16A", 10] }, then: "$P16A_OTRO", else: "$P16A" }}
                    }
                },
                {
                    $group: {    
                        "_id": "$ethnic",
                        "count": {$sum: 1}
                    }
                },
                {
                    $sort: {    
                        "count": -1
                    }
                },
                {
                    $limit: 6
                },
                {
                    $project: {
                        _id: 0,
                        "ethnic": "$_id",
                        count:1
                    }
                }
                
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }
});

// ========================================================== ACADEMIC LEVEL INDUSTRY API
function getSankeyFormatData(industryData){
    var links_arr = [];
    for (i = 0; i < industryData.length; i++) {
        var source = industryData[i]._id.level;
        var target = industryData[i]._id.industry;
        var value = industryData[i].count

        if(source == 11) source = 3;
        if(source == 12) source = 2;
        if(source == 13) source = 1;
        if(source == 14) source = 0;

        if(target == "A") target = 4;
        if(target == "B") target = 5;
        if(target == "C") target = 6;
        if(target == "D") target = 7;
        if(target == "E") target = 8;
        if(target == "F") target = 9;
        if(target == "G") target = 10;
        if(target == "H") target = 11;
        if(target == "I") target = 12;
        if(target == "J") target = 13;
        if(target == "K") target = 14;
        if(target == "L") target = 15;
        if(target == "M") target = 16;
        if(target == "N") target = 17;
        if(target == "O") target = 18;
        if(target == "P") target = 19;
        if(target == "Q") target = 20;
        if(target == "R") target = 21;
        if(target == "S") target = 22;
        if(target == "T") target = 23;
        if(target == "U") target = 24;
        if(target == "Z") target = 25;

        links_arr.push(
            {
                "source": source,
                "target": target,
                "value": value
            }
        )
        
    }
    
    var sankey_data = {
        "nodes":[
            {"node":0,"name":"PhD"},
            {"node":1,"name":"Master"},
            {"node":2,"name":"Professional"},
            {"node":3,"name":"Technical"},
    
            {"node":4,"name":"A"},
            {"node":5,"name":"B"},
            {"node":6,"name":"C"},
            {"node":7,"name":"D"},
            {"node":8,"name":"E"},
            {"node":9,"name":"F"},
            {"node":10,"name":"G"},
            {"node":11,"name":"H"},
            {"node":12,"name":"I"},
            {"node":13,"name":"J"},
            {"node":14,"name":"K"},
            {"node":15,"name":"L"},
            {"node":16,"name":"M"},
            {"node":17,"name":"N"},
            {"node":18,"name":"O"},
            {"node":19,"name":"P"},
            {"node":20,"name":"Q"},
            {"node":21,"name":"R"},
            {"node":22,"name":"S"},
            {"node":23,"name":"T"},
            {"node":24,"name":"U"}  
            ],
        "links": links_arr
        };
    
        return sankey_data;
}
app.post('/api/data/academiclevelindustry', (req, res) => {    
    
    if(isEmpty(req.body)){
        var sankey_data = getSankeyFormatData(data_academicLevelIndustry);
        res.send(sankey_data);
    }
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },                
                {
                    $match: {
                        "P15": {$in: [11,12,13,14]},
                        "P18": {$nin: ["Z", 98, 99]}
                    }
                },
                {
                    $project: {
                        "P15": 1,
                        "P18": 1
                    }
                },
                {
                    $group:{
                        "_id": {"level": "$P15", "industry": "$P18"},
                        "count": {$sum: 1}
                    }
                }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                var sankey_data = getSankeyFormatData(docs);
                res.send(sankey_data);
            });
        });        
    }



});

// ========================================================== ACADEMIC LEVEL API
app.post('/api/data/academiclevel', (req, res) => {
    if(isEmpty(req.body))
        res.send(data_academicLevel_adults);    
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },
                {
                    $match: {
                        //P15 = 98 = Nunca asistió (P13 = 3)
                        //99 = missing, ignore this one
                        "P15": {$nin: [4, 99] }, //level
                        "P09": {$gte: 18 } //Above 18 years old
                    }
                },
                {
                    $project: {
                        "P09": 1,
                        "level": {
                                $concat: [
                                    {$cond: { if: {$eq: ["$P15", 98]}, then: "1", else : "" } },
                                    {$cond: [{ $or: [ {$eq: ["$P15", 1]}, {$eq: ["$P15", 2]}, {$eq: ["$P15", 3]}] }, "2","" ]},
                                    {$cond: [{ $or: [ {$eq: ["$P15", 5]}, {$eq: ["$P15", 6]}] }, "3","" ]},
                                    {$cond: [{ $or: [ {$eq: ["$P15", 7]}, {$eq: ["$P15", 8]}, {$eq: ["$P15", 9]}, {$eq: ["$P15", 10]}] }, "4","" ]},
                                    {$cond: { if: {$eq: ["$P15", 11]}, then: "5", else : "" } },
                                    {$cond: { if: {$eq: ["$P15", 12]}, then: "6", else : "" } },
                                    {$cond: { if: {$eq: ["$P15", 13]}, then: "7", else : "" } },
                                    {$cond: { if: {$eq: ["$P15", 14]}, then: "8", else : "" } },
                                ]
                                }
                    }
                },    
                {
                    $group: {
                        "_id": { "level": "$level", "age": "$P09"},
                        "count": {$sum: 1}
                    }
                },
                {
                    $sort: {
                        "count": -1
                    }
                }                
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }
});

// ========================================================== MAP POPULATION REGION API
app.post('/api/data/mapPopulation/regions', (req, res) => {
       if(isEmpty(req.body))
        res.send(data_regions);    
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([
                {
                    $match: match
                },
                {
                    $project:{
                        "region": {$toInt: {$divide: ["$COMUNA", 1000]}}
                    }
                 },
                 {
                    $group:{
                        "_id": "$region",
                        "count": {$sum: 1}
                    }
                 },
                 {
                    $project:{
                        "_id": 0,
                        "place": "$_id",
                        "count": "$count"
                    }
                 },
                 {
                    $sort:{
                        "count": -1
                    }
                 }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);

                res.send(docs);
            });
        });        
    }

});

// ========================================================== MAP POPULATION COMMUNES API
app.post('/api/data/mapPopulation/communes', (req, res) => {
    
    if(isEmpty(req.body))
        res.send(data_regions);    
    else{
        var filters = parseFilters(req.body);
        var match = createMatchFilter(filters);
        client.connect(function(err) {
            assert.equal(null, err);
            const db = client.db(dbName);
            const collection = db.collection(collectionName);
            
            collection.aggregate([           
                {
                    $match: match
                },
                {
                    $group:{
                        "_id": "$COMUNA",
                        "count": {$sum: 1}
                    }
                },
                {
                    $project:{
                        "_id": 0,
                        "place": "$_id",
                        "count": "$count"
                    }
                },
                {
                    $sort:{
                        "count": -1
                    }
                }
            ]).toArray(function(err, docs) {
                assert.equal(err, null);
                res.send(docs);
            });
        });        
    }

});

// ========================================================== MAP TopoJson API
app.get('/api/topoJson/:regionId', (req, res) => {

    if(req.params.regionId === 'all'){
        var mapJson = topojson.feature(topojson_regions, topojson_regions.objects.places).features;
        res.send(mapJson);
    }
    else{
        var regName = "";
        if(req.params.regionId == 1) regName = "Región de Tarapacá";
        if(req.params.regionId == 2) regName = "Región de Antofagasta";
        if(req.params.regionId == 3) regName = "Región de Atacama";
        if(req.params.regionId == 4) regName = "Región de Coquimbo";
        if(req.params.regionId == 5) regName = "Región de Valparaíso";
        if(req.params.regionId == 6) regName = "Región del Libertador Bernardo O'Higgins";
        if(req.params.regionId == 7) regName = "Región del Maule";
        if(req.params.regionId == 8) regName = "Región del Biobío";
        if(req.params.regionId == 9) regName = "Región de La Araucanía";
        if(req.params.regionId == 10) regName = "Región de Los Lagos";
        if(req.params.regionId == 11) regName = "Región de Aysén del Gral.Ibáñez del Campo";
        if(req.params.regionId == 12) regName = "Región de Magallanes y Antártica Chilena";
        if(req.params.regionId == 13) regName = "Región Metropolitana de Santiago";
        if(req.params.regionId == 14) regName = "Región de Los Ríos";
        if(req.params.regionId == 15) regName = "Región de Arica y Parinacota";

        var mapJson;
        var mapSelection;
        //Santiago uses heavy map since it needs more details
        if(regName === "Región Metropolitana de Santiago")
            mapSelection = topojson_communes_heavy
        else
            mapSelection = topojson_communes_veryLight

        mapJson = topojson.feature(mapSelection, mapSelection.objects.comunas_de_chile).features;
        
        var communesList = [];
        for (i = 0; i < mapJson.length; i++) {
            if(mapJson[i].properties.nom_reg == regName 
                && mapJson[i].properties.nom_com !== "Isla de Pascua"
                && mapJson[i].properties.nom_com !== "Juan Fernández"){
                
                    communesList.push(mapJson[i]);
            }
        }

        res.send(communesList);
    }
});



function createMatchFilter(filters){
    var match = {}

    if(!isNaN(filters.genderId))
        match.P08 = filters.genderId

    if(!isNaN(filters.ageRange_from)){
        if(!isNaN(filters.ageRange_to))
            match.P09 = { $gte: filters.ageRange_from, $lte: filters.ageRange_to}
        else
            match.P09 = { $gte: filters.ageRange_from}
    }

    if(!isNaN(filters.regionId)){
        match.COMUNA = { $gte: filters.regionId * 1000, $lt: (filters.regionId + 1) * 1000}
    }

    if(!isNaN(filters.nationalityId)){
        if(filters.nationalityId == 3 ||
            filters.nationalityId == 4 ||
            filters.nationalityId == 5 ||
            filters.nationalityId == 6 ||
            filters.nationalityId == 7)
            match.P12 = filters.nationalityId
        else           
            match.P12PAIS = filters.nationalityId
    }

    if(!isNaN(filters.ethnicId)){
        if(filters.ethnicId.toString().length == 1) //high IDs, then use P16A_OTRO
            match.P16A = filters.ethnicId
        else
            match.P16A_OTRO = filters.ethnicId / 1000
    }

    return match;    
}

function parseFilters(filters){
    filters.genderId = parseInt(filters.genderId);
    filters.ageRange_from = parseInt(filters.ageRange_from);
    filters.ageRange_to = parseInt(filters.ageRange_to);
    filters.regionId = parseInt(filters.regionId);
    filters.nationalityId = parseInt(filters.nationalityId);
    filters.ethnicId = parseInt(filters.ethnicId);
    return filters;
}

function isEmpty(obj) {
    return !Object.keys(obj).length;
}

//PORT = enviorment variable
const port = process.env.PORT | 3200;
app.listen(port, () => {console.log('Server started on port ' + port + ' ...');});