# Census Project


Abstract: Working the data of an organization is an imperative in modern times to create value. What many still do not know is that not everything is about the treatment and analysis of data, but it is fundamental to make the results visible in such a way that the decision-makers implement timely measures that allow them to benefit from the opportunities or mitigate the threats. The present work aims to demonstrate that it is possible to create knowledge in a simple way through the exploration of data using interactive visualizations capable of facilitate the analysis of information. For this purpose, the data obtained from the 2017 Chilean census was used as a case of use.


* ReactJs
* D3.js
* ExpressJs
* MongoDb



This project can not be run in a local computer without the database. 
Database is not included in this repository.



/Census.Services: backend code, APIs written in ExpressJs

/Census.Client: frontend code written in ReactJs


Video explanation: https://www.youtube.com/watch?v=0Dhm7FJmu4o