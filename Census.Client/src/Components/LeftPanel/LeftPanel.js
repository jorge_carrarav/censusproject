import React, { Component } from 'react';
import "./LeftPanel.css"
import Filter from './Filter/Filter';
import '../Shared/Styles/hamburgers.min.css';
import * as d3 from "d3";

class Filters extends Component {
    constructor(props) {
        super(props)

    }
    filterButtonClick = (filters) => {
        this.props.filterButtonClick(filters)
    }
    componentDidMount  () { //OnLoadthis.buildChart(this.props.data)
        
        if(d3.select("body").node().getBoundingClientRect().width < 600) //if mobile phone, close flter
            this.openCloseFilter(true)
    }
    openCloseFilter(closeIt){
        if(document.getElementById("AppLeftContainer").className.indexOf("closed") == -1 || closeIt == true){ //so it is opened
            document.getElementById("AppLeftContainer").className = document.getElementById("AppLeftContainer").className.replace("opened", "closed");
            document.getElementById("AppRightContainer").className = document.getElementById("AppRightContainer").className.replace("opened", "closed");
            document.getElementById("OpenCloseFilterButton").className = document.getElementById("OpenCloseFilterButton").className.replace("opened", "closed");            
            document.getElementById("TitleContainer").className = document.getElementById("TitleContainer").className.replace("opened", "closed");            
        }
        else
        {
            document.getElementById("AppLeftContainer").className = document.getElementById("AppLeftContainer").className.replace("closed", "opened");
            document.getElementById("AppRightContainer").className = document.getElementById("AppRightContainer").className.replace("closed", "opened");
            document.getElementById("OpenCloseFilterButton").className = document.getElementById("OpenCloseFilterButton").className.replace("closed", "opened");     
            document.getElementById("TitleContainer").className = document.getElementById("TitleContainer").className.replace("closed", "opened");
        }
    }
    
    render() {
        return (
            <div id="LeftPanel" className="LeftPanel">
                <div id="TitleContainer" className="TitleContainer_Filter_opened">
                        <div id="OpenCloseFilterButton" className="OpenCloseFilterButton_Filter_opened" onClick={this.openCloseFilter}>
                            <div className="bar1"></div>
                            <div className="bar2"></div>
                            <div className="bar3"></div>
                        </div>
                    
                    <div className="AppTitle">
                        Census Chile 2017
                    </div>
                </div>
                <Filter filterButtonClick={this.filterButtonClick}></Filter>
            </div>
        );
    }
}

export default Filters;