import React, { Component } from 'react';

import { getString } from '../../../i18n/i18n'
import * as Dictionary from '../../Shared/Dictionary.js'
import * as FilterOptions from '../../Shared/FilterOptions'

import './Filter.css'
import { Label } from 'react-bootstrap';
class Filter extends Component {
    constructor(props) {
        super(props)

    }
    componentDidMount(){
        this.loadOptions();
    }
    loadOptions(){
        var regionOptions = FilterOptions.Regions();
        var ddl_region = document.getElementById("ddl_region");
        for(var i=0; i<regionOptions.length;i++){
            var option = document.createElement("option");
            option.text = regionOptions[i];
            ddl_region.add(option)
        }        
        
        var nationalityOptions = FilterOptions.Countries();
        var ddl_nationality = document.getElementById("ddl_nationality");
        for(var i=0; i<nationalityOptions.length;i++){
            var option = document.createElement("option");
            option.text = nationalityOptions[i];
            ddl_nationality.add(option)
        }    
        
        var ethnicOptions = FilterOptions.Ethnics();
        var ddl_ethnic = document.getElementById("ddl_ethnic");
        for(var i=0; i<ethnicOptions.length;i++){
            var option = document.createElement("option");
            option.text = ethnicOptions[i];
            ddl_ethnic.add(option)
        }
        

    }
    
    filterButtonClick = () => {
        
        var filters = this.getFilters();
        
        this.props.filterButtonClick(filters); //call APIs
    }
    clearFilterButtonClick = () => {
        document.getElementById("ddl_gender").selectedIndex = 0;
        document.getElementById("ddl_ageRange").selectedIndex = 0;
        document.getElementById("ddl_region").selectedIndex = 0;
        document.getElementById("ddl_nationality").selectedIndex = 0;
        document.getElementById("ddl_ethnic").selectedIndex = 0;

        document.getElementById("btn_clearFilter").style.visibility = 'hidden';

        this.filter_gender_onChange();
        this.filter_ageRange_onChange();
        this.filter_region_onChange();
        this.filter_nationality_onChange();
        this.filter_ethnic_onChange();
        var filters = {};
        this.props.filterButtonClick(filters); //call APIs
    }
    getFilters(){

        var ddl_gender = document.getElementById("ddl_gender");
        var ddl_ageRange = document.getElementById("ddl_ageRange");
        var ddl_region = document.getElementById("ddl_region");
        var ddl_nationality = document.getElementById("ddl_nationality");
        var ddl_ethnic = document.getElementById("ddl_ethnic");

        if(ddl_gender.selectedIndex == 0 &&
            ddl_ageRange.selectedIndex == 0 &&
            ddl_region.selectedIndex == 0 &&
            ddl_nationality.selectedIndex == 0 &&
            ddl_ethnic.selectedIndex == 0){
                var btn_clearFilter = document.getElementById("btn_clearFilter").style.visibility = 'hidden';
                return {}
        }
        else {
            var btn_clearFilter = document.getElementById("btn_clearFilter").style.visibility = 'visible';

            var selectedGender = ddl_gender.options[ddl_gender.selectedIndex].value
                                .replace(getString("gender") + ": ", "" )
                                .replace(getString("all_os"), "all" )
                                .replace(getString("all_as"), "all" );
            
            var selectedAgeRange = ddl_ageRange.options[ddl_ageRange.selectedIndex].value
                                .replace(getString("age") + ": ", "" )
                                .replace(getString("all_os"), "all" )
                                .replace(getString("all_as"), "all" );
            
            var selectedRegion = ddl_region.options[ddl_region.selectedIndex].value
                                .replace(getString("region") + ": ", "" )
                                .replace(getString("all_os"), "all" )
                                .replace(getString("all_as"), "all" );
            
            var selectedNationality = ddl_nationality.options[ddl_nationality.selectedIndex].value
                                .replace(getString("nationality") + ": ", "" )
                                .replace(getString("all_os"), "all" )
                                .replace(getString("all_as"), "all" );
            
            var selectedEthnic = ddl_ethnic.options[ddl_ethnic.selectedIndex].value
                                .replace(getString("ethnic") + ": ", "" )
                                .replace(getString("all_os"), "all" )
                                .replace(getString("all_as"), "all" );
            

            var genderId = "";
            var ageRange_from = "";
            var ageRange_to = "";
            var regionId = "";
            var nationalityId = "";
            var ethnicId = "";
            
            if(selectedGender != "all")
                genderId = selectedGender == getString("men")? 1: 2;
            
            if(selectedAgeRange != "all"){
                ageRange_from = parseInt(selectedAgeRange.split(" - ")[0]);
                if(ageRange_from != 71)
                    ageRange_to = parseInt(selectedAgeRange.split(" - ")[1]);
            }

            if(selectedRegion != "all")
                regionId = Dictionary.RegionsAndCommunesId_byValue(selectedRegion)
            
            if(selectedNationality != "all")
                nationalityId = Dictionary.CountryId_byValue(selectedNationality)
            
            if(selectedEthnic != "all")
                ethnicId = Dictionary.EthnicId_byValue(selectedEthnic)
            
            var filters = {
                            "genderId": genderId,
                            "ageRange_from": ageRange_from,
                            "ageRange_to": ageRange_to,
                            "regionId": regionId,
                            "nationalityId": nationalityId, 
                            "ethnicId": ethnicId
                        }
                return filters;
        }
    }
    filter_gender_onChange(){
        if(document.getElementById('ddl_gender').selectedIndex  != 0) {
            document.getElementById("div_hlFilter_gender").className = "div_highlightFilter";
        }
        else{
            document.getElementById("div_hlFilter_gender").className = "";
        }
    }
    filter_ageRange_onChange(){
        if(document.getElementById('ddl_ageRange').selectedIndex  != 0) {
            document.getElementById("div_hlFilter_ageRange").className = "div_highlightFilter";
        }
        else{
            document.getElementById("div_hlFilter_ageRange").className = "";
        }
    }
    filter_region_onChange(){
        if(document.getElementById('ddl_region').selectedIndex  != 0) {
            document.getElementById("div_hlFilter_region").className = "div_highlightFilter";
        }
        else{
            document.getElementById("div_hlFilter_region").className = "";
        }
    }
    filter_nationality_onChange(){
        if(document.getElementById('ddl_nationality').selectedIndex  != 0) {
            document.getElementById("div_hlFilter_nationality").className = "div_highlightFilter";
        }
        else{
            document.getElementById("div_hlFilter_nationality").className = "";
        }
    }
    filter_ethnic_onChange(){
        if(document.getElementById('ddl_ethnic').selectedIndex  != 0) {
            document.getElementById("div_hlFilter_ethnic").className = "div_highlightFilter";
        }
        else{
            document.getElementById("div_hlFilter_ethnic").className = "";
        }
    }
    render() {
        return (
            <div id="FilterContainer" >

                <div className="form-group">

                    <br></br>
                
                <div id="div_hlFilter_gender">
                    <select id="ddl_gender" className="form-control" onChange={this.filter_gender_onChange}>
                        <option>{getString("gender")}: {getString("all_os")} </option>
                        <option>{getString("men")}</option>
                        <option>{getString("women")}</option>
                    </select>
                </div>
                    <br></br><br></br>

                <div id="div_hlFilter_ageRange">
                    <select id="ddl_ageRange" className="form-control" onChange={this.filter_ageRange_onChange}>
                        <option>{getString("age")}: {getString("all_as")}</option>
                        <option>1 - 10</option>
                        <option>11 - 20</option>
                        <option>21 - 30</option>
                        <option>31 - 40</option>
                        <option>41 - 50</option>
                        <option>51 - 60</option>
                        <option>51 - 70</option>
                        <option>71+</option>
                    </select>
                </div>
                    <br></br><br></br>

                <div id="div_hlFilter_region">
                    <select id="ddl_region" className="form-control" onChange={this.filter_region_onChange}>
                        <option>{getString("region")}: {getString("all_as")}</option>
                    </select>
                </div>
                    <br></br><br></br>

                <div id="div_hlFilter_nationality">
                    <select id="ddl_nationality" className="form-control" onChange={this.filter_nationality_onChange}>
                        <option>{getString("nationality")}: {getString("all_as")}</option>
                    </select>
                </div>
                    <br></br><br></br>

                <div id="div_hlFilter_ethnic">
                    <select id="ddl_ethnic" className="form-control" onChange={this.filter_ethnic_onChange}>
                        <option>{getString("ethnic")}: {getString("all_as")}</option>
                    </select>
                </div>
                    <br></br><br></br>

                    <button onClick={this.filterButtonClick} className="btn btn-success" type="button">
                        {getString("filter")}
                    </button>
                    <br></br><br></br>
                    
                    <button id="btn_clearFilter" onClick={this.clearFilterButtonClick} className="btn btn-primary" style={{visibility: "hidden"}}  type="button">
                        {getString("clearFilter")}
                    </button>
                </div>
                <div id="div_txt_totalPopulation">
                    <Label id="txt_totalPopulation"></Label>
                    <Label id="txt_people"> {getString("people").toLowerCase()} </Label>
                </div>

            </div>
        );
    }
}

export default Filter;