import React, { Component } from 'react';
import "./Dashboard.css"
import config from '../../../config.json'
import { getString } from '../../../i18n/i18n'
import MapPopulation from './Widgets/MapPopulation/MapPopulation';
import Gender from './Widgets/Gender/Gender';
import AgeRange from './Widgets/AgeRange/AgeRange';
import Foreigner from './Widgets/Foreigner/Foreigner';
import Ethnic from './Widgets/Ethnic/Ethnic';
import PopulationComparison from './Widgets/PopulationComparison/PopulationComparison';
import AcademicLevel from './Widgets/AcademicLevel/AcademicLevel';
import AcademicLevelIndustry from './Widgets/AcademicLevelIndustry/AcademicLevelIndustry';
import * as d3 from "d3";



class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            update: 1
        }
        //Preparing tooltip
        d3.select("body")
            .append("div")
            .attr("id", "d3tooltipContainer")
            .style("opacity", "0")
            .append("div")
            .attr("id", "d3tooltip")


        this.genderComponent = React.createRef();
        this.ageRangeComponent = React.createRef();
        this.foreignerComponent = React.createRef();
        this.ethnicComponent = React.createRef();
        this.academicLevelIndustryComponent = React.createRef();
        this.academicLevelComponent = React.createRef();
        this.mapPopulationComponent = React.createRef();
        this.populationComparisonComponent = React.createRef();


        this.populationData = this.populationData.bind(this)

        

    }

    filterButtonClick(filters){        
        this.genderComponent.current.filterButtonClick(filters);
        this.ageRangeComponent.current.filterButtonClick(filters);
        this.foreignerComponent.current.filterButtonClick(filters);
        this.ethnicComponent.current.filterButtonClick(filters);
        this.academicLevelIndustryComponent.current.filterButtonClick(filters);
        this.academicLevelComponent.current.filterButtonClick(filters);
        this.populationComparisonComponent.current.filterButtonClick(filters);
        this.mapPopulationComponent.current.filterButtonClick(filters);
    }

    populationData(data){        
        this.populationComparisonComponent.current.populationData(data)
    }


    
   
    render() {

            return (
                <div className="Dashboard"> 
                    <div className="WidgetsGroup WidgetsGroup_1">
                        <div className="row">                        
                                
                            <div className="col-lg-8">
                                <div className="row" >
                                    <Gender className="col-lg-6" ref={this.genderComponent}></Gender>
                                    <AgeRange className="col-lg-6" ref={this.ageRangeComponent}></AgeRange>
                                    <Foreigner className="col-lg-6" ref={this.foreignerComponent}></Foreigner>
                                    <Ethnic className="col-lg-6" ref={this.ethnicComponent}></Ethnic>
                                </div>
                            </div>                            
                            <div className="col-lg-4">
                                <div className="row">
                                    <MapPopulation className="col-lg-12" ref={this.mapPopulationComponent} populationData={this.populationData}></MapPopulation>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="WidgetsGroup">                    
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="row" >
                                    <AcademicLevelIndustry className="col-lg-12" ref={this.academicLevelIndustryComponent}></AcademicLevelIndustry>
                                </div>
                            </div>   
                            <div className="col-lg-4">
                                <div className="row">
                                    <PopulationComparison className="col-lg-12" ref={this.populationComparisonComponent}></PopulationComparison>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="row" >
                                    <AcademicLevel className="col-lg-12" ref={this.academicLevelComponent}></AcademicLevel>                              
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div className="Footer col-lg-12">
                        <br></br>
                    </div>
                </div>
            );
        
    }

}

export default Dashboard;
