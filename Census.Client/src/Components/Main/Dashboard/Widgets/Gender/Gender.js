import React, { Component } from 'react';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n.js'
import * as Helper from '../../../../Shared/Helper.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import WidgetSimple from '../Shared/WidgetSimple/WidgetSimple';
import './Gender.css'
import * as d3 from "d3";

class Gender extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("gender"),
            chart:(
                <svg id="svg_gender" className="SVG"></svg>
            ),
            loaded: false,
        }

    }    
    componentDidMount  () { //OnLoad
        this.callAPI();
    }
    callAPI(filters){    
        document.getElementById("txt_totalPopulation").innerHTML = "";
        document.getElementById("txt_people").innerHTML = "";

        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/gender", options)
        .then(res => res.json())
        .then((data) => {
                
                this.setState({
                    loaded: true
                });
                
                var totalPopulation;
                if(data.length > 1)
                    totalPopulation = data[0].count + data[1].count;
                else   
                    totalPopulation = data[0].count;
                document.getElementById("txt_totalPopulation").innerHTML  = Helper.thousandSeparator(totalPopulation)
                document.getElementById("txt_people").innerHTML = " " + getString("people").toLowerCase();

                this.buildChart(data);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        this.callAPI(filters);        
    }
    

    buildChart(data){
        var svgWidth = document.getElementById('svg_gender').clientWidth;
        var svgHeight = document.getElementById('svg_gender').clientHeight;
        var svg = d3.select('#svg_gender').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg
            .append('g')
        
        var tooltip_fade = config.animation.tooltip.fade;
        var tooltip_movement = config.animation.tooltip.movement;
        //--------------- Must have
        
        var chartProps = {
            marginTop: 50,
            marginBottom: 50,
            barSeparation: 160,
            barWidth: 120,
        }

        var scaleSize = d3.scaleLinear()
                .domain([0, d3.max(data, d => d.count)])
                .range([0, svgHeight - chartProps.marginTop]);   

        var scaleColor = d3.scaleOrdinal()
                .domain(["men", "women"])
                .range(["#3281B8", "#FA8823"]);
        

                
        var minPeople = d3.min(data, d => d.count)
        var maxPeople = d3.max(data, d => d.count)
        var totalPeople = minPeople + maxPeople;
        //Bars
        gContainer            
            .selectAll("rect")
            .data(data)
            .enter()
            .append("rect")
            .attr("x", (d,i) => i* chartProps.barSeparation) 
            .attr("y", d => svgHeight - scaleSize(d.count) - chartProps.marginBottom)
            .attr("width", chartProps.barWidth)
            .attr("height", d => scaleSize(d.count))
            .attr("fill", d=> scaleColor(d.gender) )
            .on('mouseover', function (d, i) { //--- Mouse over
                
                //Highlight
                d3.select(this)
                    .attr("fill", config.colors.highlight)
                //show tooltip
                showTooltip(d,i);
            })
            .on('mouseout', function (d, i) { //--- Mouse out
                //color back
                d3.select(this)
                    .attr("fill", d=> scaleColor(d.gender) )

                Tooltip.hideTooltip();
            });
        
        var text = gContainer
                .selectAll("text")
                .data(data)
                .enter()
        text
            .append("text")
            .attr("cursor", "default")    
            .attr("fill", "white")
            .attr("x", (d,i) => i * chartProps.barSeparation + chartProps.barWidth/2 - 28)
            .attr("y", d => svgHeight - chartProps.marginBottom + 20)
            .text(d => getString(d.gender))
        text
            .append("text")
            .attr("fill", "white")
            .attr("cursor", "default")
            .attr("x", (d,i) => i * chartProps.barSeparation + chartProps.barWidth/2 - 38)
            .attr("y", d => svgHeight - chartProps.marginBottom - 50)
            .attr("font-size", 30)
            .text(d => (Helper.limitDecimals(d.count * 100 / totalPeople, 1)) + "%")

        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gContainer);
        gContainer.attr("transform", "translate(" + alignMiddle[0] + " " + alignMiddle[1] + ") ")
        

        
        //show tooltip
        function showTooltip(d,i){            
            //show title
            Tooltip.showTooltip_andHeader(getString(d.gender))

            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.count * 100 / config.country.people2017, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])
        }

    }
    render() {
        if (this.state.loaded == false)
            return (    
                <div className={"NoPadding " + this.props.className}>
                    <WidgetSimple title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetSimple>
                </div>           
            )
        else{
            return (
                <div className={"NoPadding " + this.props.className}>
                    <WidgetSimple title={this.state.title} chart={this.state.chart}></WidgetSimple>
                </div>
            );
        }
    }
}

export default Gender;