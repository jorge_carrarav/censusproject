import React, { Component } from 'react';
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n'
import './Commune.css'
import * as Helper from '../../../../Shared/Helper.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import * as d3 from "d3";


class Commune extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("communes"),
            subtitle:  "top 15" + " (" + getString("thousands") + ")",
            chart:(
                <svg id="svg_communes" className="SVG"></svg>
            ),
            data: this.props.data
        }
        
    }
    componentDidMount  () { //OnLoad
        this.buildChart(this.state.data);
    }
    buildChart(data){
        var svgWidth = document.getElementById('svg_communes').clientWidth;
        var svgHeight = document.getElementById('svg_communes').clientHeight;
        
        var svg = d3.select('#svg_communes').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg.append('g')

        //--------------- Must have

        
        var chartProps = {
            axis: {
                color: "white",//"#F7F2A3",
                fontSize: 20,
                fontWeight: "bold",
                pos_leftText: -60, 
                pos_rightText: 20,
                marginTop: 50,
            },
            marginTop: 100,
            marginBottom: 30,
            barSeparation: 120,
            textLineSeparation: 15
        }
        var barSeparation = d3.select("body").node().getBoundingClientRect().width < 600 ? 70 : chartProps.barSeparation;

        var maxLast = d3.max(data, d=> {
            return d.people.last})
        var maxOld = d3.max(data, d=> d.people.old)
        var minLast = d3.min(data, d=> d.people.last)
        var minOld = d3.min(data, d=> d.people.old)        
        var posScale = d3.scaleLinear()
                    .domain([d3.min([minLast,minOld]), d3.max([maxLast,maxOld])])
                    .range([svgHeight - chartProps.marginBottom, chartProps.marginTop])

        var elements = gContainer
                    .selectAll("text")
                    .data(data)
                    .enter()
          
        //2012 elements Commue
        elements
            .append("text")
            .attr("class", d => d.place.replace(/ /g,''))
            .attr("x", 0)
            .attr("y", d => posScale(d.people.old))
            .attr("fill", "white")
            .attr("text-anchor", "end")
            .style("cursor", "default")
            .text(d => d.place + " (" + Helper.limitDecimals(d.people.old / 1000, 0) + " mil)")
            .on("mouseover", function (d,i) {            
                //highlight 
                d3.selectAll("." + d.place.replace(/ /g,''))
                    .attr("fill", config.colors.highlight)   


                //show tooltip
                showTooltip(d,i)
            })
            .on("mouseout", function (d,i) {
                //highlight 
                d3.selectAll("." + d.place.replace(/ /g,''))
                    .attr("fill", "white")

                //hide tooltip
                Tooltip.hideTooltip()
            })
            
        
        //2017 elements Commune
        elements
            .append("text")
            .attr("class", d => d.place.replace(/ /g,''))
            .attr("fill", "white")
            .attr("x", barSeparation)
            .attr("y", d => posScale(d.people.last))
            .attr("text-anchor", "init")
            .style("cursor", "default")
            .text(d => "(" + Helper.limitDecimals(d.people.last / 1000, 0) + " mil)" + " " + d.place)
            .on("mouseover", function (d,i) {    
                //highlight 
                d3.selectAll("." + d.place.replace(/ /g,''))
                    .attr("fill", config.colors.highlight)    
                    
                
                //show tooltip
                showTooltip(d,i)
            })
            .on("mouseout", function (d,i) {
                //highlight 
                d3.selectAll("." + d.place.replace(/ /g,''))
                    .attr("fill", "white")
                //hide tooltip
                Tooltip.hideTooltip()
            }) 
        //Lines
        elements
            .append("line")
            .attr("stroke", d => {
                if(d.people.old > d.people.last)
                    return "red"
                else
                    return "green"
            })
            .attr("stroke-width", 2)
            .attr("x1", chartProps.textLineSeparation)
            .attr("y1", d => posScale(d.people.old) - 3)
            .attr("x2", barSeparation - chartProps.textLineSeparation)
            .attr("y2", d => posScale(d.people.last) - 3)
            .on("mouseover", (d,i) => {
                
                //show tooltip
                showTooltip(d,i)
            })
            .on("mouseout", (d,i) => {
                //hide tooltip
                Tooltip.hideTooltip()
            })

        //Legend
        gContainer
            .append("text")
            .attr("fill",chartProps.axis.color)
            .attr("font-size", chartProps.axis.fontSize)
            .attr("font-weight", chartProps.axis.fontWeight)
            .attr("x", chartProps.axis.pos_leftText)
            .attr("y", chartProps.axis.marginTop)
            .text("2012")
        gContainer
            .append("text")
            .attr("fill", chartProps.axis.color)
            .attr("font-size", chartProps.axis.fontSize)
            .attr("font-weight", chartProps.axis.fontWeight)
            .attr("x", barSeparation + chartProps.axis.pos_rightText)
            .attr("y",  chartProps.axis.marginTop)
            .text("2017")
        
        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gContainer);
        gContainer.attr("transform", "translate(" + alignMiddle[0] + " " + alignMiddle[1] + ") ")



        // show Tooltip ---------------
        function showTooltip(d,i){
            //show title
            Tooltip.showTooltip_andHeader(d.place)

            //text - 2012
            Tooltip.addNewLine([
                {"text":"-- 2012 --", "fontWeight": "bold"}             
            ])     

            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.people.old), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.people.old * 100 / config.country.people2012, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])
          
            Tooltip.addNewLine()


            //text 2017
            Tooltip.addNewLine([
                {"text":"-- 2017 --", "fontWeight": "bold"}             
            ])     

            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.people.last), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.people.last * 100 / config.country.people2017, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])

        }
    }
    render() {
        return (
            <div className={"NoPadding  " + this.props.className}>
                <WidgetDouble title={this.state.title} 
                                subtitle={this.state.subtitle}
                                chart={this.state.chart}></WidgetDouble>
            </div>
        );
    }
}

export default Commune;