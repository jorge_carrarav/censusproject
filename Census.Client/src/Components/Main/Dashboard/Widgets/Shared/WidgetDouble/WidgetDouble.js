import React, { Component } from 'react';
import WidgetSimple from "../WidgetSimple/WidgetSimple"
import "./WidgetDouble.css"

class WidgetDouble extends Component {

    render() {
        return (
            <div className="WidgetDouble">
                <WidgetSimple title={this.props.title}
                                subtitle={this.props.subtitle}
                                 chart={this.props.chart}></WidgetSimple>
            </div>
        );
    }
}

export default WidgetDouble;