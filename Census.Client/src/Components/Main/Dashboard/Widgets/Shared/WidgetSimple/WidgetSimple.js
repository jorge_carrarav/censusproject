import React, { Component } from 'react';
import "./WidgetSimple.css"

class WidgetSimple extends Component {

    render() {
        return (
            <div className={"WidgetSimple"}>
                <div className="WidgetTitleContainer">
                    <div className="row">
                        <div className="WidgetTitle col-10">
                            {this.props.title}
                        </div>
                        <div className="Subtitle col-2">
                            {this.props.subtitle}
                        </div>
                    </div>
                </div>
                <div className="ChartContainer">
                    {this.props.chart}
                </div>
            </div>
        );
    }
}

export default WidgetSimple;