import React, { Component } from 'react';
import { getString } from '../../../../../i18n/i18n'
import config from '../../../../../config.json'
import * as Helper from '../../../../Shared/Helper.js'
import * as Dictionary from '../../../../Shared/Dictionary.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import './AcademicLevel.css'
import * as d3 from "d3";
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import Dictionary_academicLevel from '../../../../../Dictionary/academicLevel.json'

class AcademicLevel extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("academiclevelByAge"),
            subtitle: getString("above18YearsOld"),
            chart:(
                <svg id="svg_academicLevel" className="SVG">
                </svg>
            ),
            loaded: false

        }
        
    }
    componentDidMount  () { //OnLoad
        this.callAPI();
    }
    callAPI(filters){
        
        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/academiclevel", options)
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    loaded: true
                });

                this.buildChart(data);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        this.callAPI(filters);        
    }

    buildChart(data){
        var svgWidth = document.getElementById('svg_academicLevel').clientWidth;
        var svgHeight = document.getElementById('svg_academicLevel').clientHeight;
        
        var svg = d3.select('#svg_academicLevel').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg.append('g')
        var gRowContainer = gContainer.append('g')

        //--------------- Must have   


        var ageMin = d3.min(data, d => d._id.age)
        var ageMax = d3.max(data, d => d._id.age)

        var chartProps = {
            margin: {
                top: 0,
                left: 200,
                right: 80,
                bottom: 50
            },
            circle: {
                minR: 5,
                maxR: 70,
                separationCircles: 65,
                separationAxisX: 0,
                separationAxisY: 60,
                opacity: 0.5
            },
            age: {
                min: ageMin,
                max: ageMax
            }
        }
        
        var peopleMin = d3.min(data, d => d.count)
        var peopleMax = d3.max(data, d => d.count)


        var radiousScale = d3.scaleLinear()
                    .domain([peopleMin, peopleMax])
                    .range([chartProps.circle.minR, chartProps.circle.maxR])

        var posXScale = d3.scaleLinear()
            .domain([chartProps.age.min, chartProps.age.max])
            .range([chartProps.circle.separationAxisY + chartProps.margin.left
                ,svgWidth - chartProps.margin.right])


        function posYScale(index){
            return svgHeight - chartProps.margin.bottom - chartProps.circle.separationAxisX - (chartProps.circle.separationCircles * index);
        }


        var colorScale = d3.scaleLinear()
                .domain([0 ,peopleMax/200, peopleMax/20, peopleMax])
                .range(["#171717", "gray", "red", "red"])
                //.range(["black", "gray", "red", "red"])

        //Circles
        gContainer
            .selectAll("circle")
            .data(data)
            .enter()
            .append("circle")
            .attr("cx", d => posXScale(d._id.age))
            .attr("cy", d => posYScale(d._id.level) - 7)
            .attr("r", d => radiousScale(d.count))
            .attr("fill", d => colorScale(d.count))
            .attr("opacity", chartProps.circle.opacity)
            .on('mouseover', function (d, i) {

                //highlight circle
                d3.select(this)
                    .attr("fill", config.colors.highlight)  
                //highlight text
                d3.select("." + Dictionary.AcademicLevel(d._id.level))
                    .attr("fill", config.colors.highlight)  

                //show tooltip
                showTooltip(d)                     
            })
            .on('mouseout', function (d, i) {
                //highlight circle
                d3.select(this)
                    .attr("fill", d => colorScale(d.count))    
                //highlight text
                d3.select("." + Dictionary.AcademicLevel(d._id.level))
                    .attr("fill", "white")  

                //hide tooltip
                Tooltip.hideTooltip()
            })

        
        var xAxisTicks = []
        var xAxis;
        var ddl_ageRange = document.getElementById("ddl_ageRange")
        if(ddl_ageRange.selectedIndex != 0)
        {
            var min = ddl_ageRange.options[ddl_ageRange.selectedIndex].value.split(" - ")[0];
            var max = ddl_ageRange.options[ddl_ageRange.selectedIndex].value.split(" - ")[1];

            for(var i = min; i <= max; i++)
                xAxisTicks.push(parseInt(i))

            xAxis = d3.axisBottom(posXScale)
                        .tickValues(xAxisTicks)
        }
        else
            xAxis = d3.axisBottom(posXScale)

        // x Axis
        gContainer
            .append("g")
            .attr("fill", "red")
            .style("font", "14px times")
            .attr("transform", "translate(" + 0 + " " + (svgHeight - chartProps.margin.bottom) +  ")")
            .call(xAxis) //it's the same to say: axisX(g)

        // y Axis
        gContainer
            .append("g")
            .selectAll("text")
            .data(Dictionary_academicLevel)
            .enter()
            .append("text")
            .attr("x", chartProps.margin.left)
            .attr("y", d => posYScale(d.id) )
            .attr("fill","white")
            .attr("class", d => d.value) 
            .attr("stroke-width", 0)
            .attr("text-anchor", "end")
            .attr("cursor", "default")            
            .text(d => getString(d.value))
            .on('mouseover', function (d, i) {
                //highlight
                d3.select(this)
                    .attr("fill", config.colors.highlight)
            })
            .on('mouseout', function (d, i) {
                //highlight
                d3.select(this)
                    .attr("fill", "white")                    
            })



        //tooltip
        function showTooltip(d){
            //show title
            Tooltip.showTooltip_andHeader(getString(Dictionary.AcademicLevel(d._id.level)))

              
            //text
            Tooltip.addNewLine([
                {"text": d._id.age, "fontWeight": "bold"},    
                {"text": getString("years"), "fontWeight": "normal"}        
            ])
            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])
        }
    }
    render() {
        if (this.state.loaded == false)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title} 
                                    chart={(
                                            <div className="Loading"></div>
                                    )}></WidgetDouble>
                </div>  
            )
        else{
            return (
                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title}
                                    subtitle={this.state.subtitle}
                                    chart={this.state.chart}></WidgetDouble>
                </div>
            );
        }
    }
}

export default AcademicLevel;