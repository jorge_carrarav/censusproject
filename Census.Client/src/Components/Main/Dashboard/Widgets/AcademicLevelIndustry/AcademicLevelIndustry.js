import React, { Component } from 'react';
import config from '../../../../../config.json'
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import { getString } from '../../../../../i18n/i18n'
import './AcademicLevelIndustry.css'
import {sankey as d3Sankey} from './sankey.js'
import * as Helper from '../../../../Shared/Helper.js'
import * as Dictionary from '../../../../Shared/Dictionary.js'
import * as d3 from "d3";

class AcademicLevelIndustry extends Component {
    constructor(props) {
        super(props);
        this.state = 
        {
            title: getString("industryByAcademicLevel"),
            subtitle: getString(""),
            chart:(
                <svg id="svg_employabilityIndustry" className="SVG">
                </svg>
            ),
            loaded: false

        }
        
    }
    componentDidMount  () { //OnLoad
        this.callAPI();
    }
    callAPI(filters){
        
        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/academiclevelindustry", options)
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    loaded: true
                });
                this.buildChart(data);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        this.callAPI(filters);        
    }


    buildChart(data) {
        var svgWidth = document.getElementById('svg_employabilityIndustry').clientWidth;
        var svgHeight = document.getElementById('svg_employabilityIndustry').clientHeight;
        
        var svg = d3.select('#svg_employabilityIndustry').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg.append('g')
        //--------------- Must have 


        // set the dimensions and margins of the graph
        var chartProps = {
            margin: {
                top: 20,
                right: 50,
                bottom: 50,
                left: 50
            },
            node: {
                width: 50,
                separation: 10,

            }
        }


        // format variables
        var color = d3.scaleOrdinal(d3.schemeCategory10);


        // Set the sankey diagram properties
        var sankey = d3Sankey()
            .nodeWidth(chartProps.node.width)
            .nodePadding(chartProps.node.separation)
            .size([svgWidth - chartProps.margin.left - chartProps.margin.right, svgHeight - chartProps.margin.bottom])
            .nodes(data.nodes)
            .links(data.links)
            .layout(32);
        
        var path = sankey.link();

        
        // add in the links
        var link = gContainer
                    .selectAll(".link")
                    .data(data.links)
                    .enter()
                    .append("path")
                    .attr("class", "link")
                    .attr("d", path)
                    .style("stroke-width", d => { return Math.max(1, d.dy); })
                    .sort(function(a, b) { return b.dy - a.dy; });
    
        // Link tooltip
        link.append("title")
            .text(d => {
                return d.source.name + " → " + 
                    Dictionary.Industry(d.target.name).description + "\n" +
                    Helper.thousandSeparator(d.value) + " " + getString("people") });
    
        // add in the nodes
        var node = gContainer
                .append("g")
                .selectAll(".node")
                .data(data.nodes)
                .enter()
                .append("g")
                .attr("class", "node")
                .attr("transform", d => { 
                    return "translate(" + d.x + "," + d.y + ")"; })
                .call(d3.drag()
                    .subject(d => d)
                    .on("start", function() {
                        this.parentNode.appendChild(this);
                    })
                    .on("drag", dragmove));
    
        // Rectangles for the nodes
        node
            .append("rect")
            .attr("height", d => d.dy)
            .attr("width", sankey.nodeWidth())
            .style("fill", d => { 
                return d.color = color(d.name.replace(/ .*/, "")); })
            .style("stroke", d => { 
                return d3.rgb(d.color).darker(2); })
            .append("title")
            .text(d => {
                var name = d.name.length == 1? Dictionary.Industry(d.name).description: d.name;
                return name + "\n" +
                Helper.thousandSeparator(d.value) + " " + getString("people") });
    
        // Visible text per Rectangle
        node.append("text")
            .attr("x", -6)
            .attr("y", d => d.dy / 2)
            .attr("dy", ".35em")
            .attr("text-anchor", "end")
            .attr("transform", null)
            .text(d => d.name.length == 1? Dictionary.Industry(d.name).value: d.name)
            .filter(d => d.x < svgWidth / 2)
            .attr("x", 6 + sankey.nodeWidth())
            .attr("text-anchor", "start");
    
    // the function for moving the nodes
        function dragmove(d) {
        d3.select(this)
            .attr("transform", 
                "translate(" 
                    + d.x + "," 
                    + (d.y = Math.max(
                        0, Math.min(svgHeight - d.dy - chartProps.margin.bottom, d3.event.y))
                        ) + ")");
        sankey.relayout();
        link.attr("d", path);
        }

         
        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gContainer);
        gContainer.attr("transform", "translate(" + alignMiddle[0] + " " + (alignMiddle[1] + chartProps.margin.top) + ") ")
          
    }


    render() {
        if (this.state.loaded == false)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetDouble>
                </div> 
            )
        else{
            return(

                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title}
                                    subtitle={this.state.subtitle}
                                    chart={this.state.chart}></WidgetDouble>
                </div>
            );
        }
    }

}

export default AcademicLevelIndustry;