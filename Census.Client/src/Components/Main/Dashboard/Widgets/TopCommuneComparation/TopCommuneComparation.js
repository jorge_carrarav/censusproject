import React, { Component } from 'react';
import WidgetSimple from '../Shared/WidgetSimple/WidgetSimple';
import { getString } from '../../../../../i18n/i18n'
import './TopCommuneComparation.css'
import * as d3 from "d3";

class TopCommuneComparation extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("topNcommunes").replace("<num>", "10"),
            chart:(
                <svg id="svg_ethnics" className="SVG">
                </svg>
            )

        }

        
    }
    componentDidMount  () { //OnLoad
        this.buildChart()
    }
    buildChart(){

    }
    render() {
        return (
            <div className={"NoPadding " + this.props.className}>
                <WidgetSimple title={this.state.title}
                                 chart={this.state.chart}></WidgetSimple>
            </div>
        );
    }
}

export default TopCommuneComparation;