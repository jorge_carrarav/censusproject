import React, { Component } from 'react';
import WidgetSimple from '../Shared/WidgetSimple/WidgetSimple';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n'
import * as Helper from '../../../../Shared/Helper.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'

import './AgeRange.css'
import * as d3 from "d3";

class AgeRange extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("age"),
            chart:(
                <svg id="svg_age" className="SVG"></svg>
            ),
            loaded: false
        }
        
    }
    componentDidMount  () { //OnLoad
        this.callAPI()
    }
    callAPI(filters){
        
        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/agerange", options)
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    loaded: true
                });
                this.buildChart(data);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        this.callAPI(filters);        
    }
    

    buildChart(data){
        var svgWidth = document.getElementById('svg_age').clientWidth;
        var svgHeight = document.getElementById('svg_age').clientHeight;
        var svg = d3.select('#svg_age').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg
                            .append('g')
                            .attr("id", "gContainer_age")
        
        //--------------- Must have
        
        var margin = {
            top: 0,
            left: 100,
            right: 25,
            bottom: 20
        };

        var gWidth = svgWidth - margin.left - margin.right,
            gHeight = svgHeight - margin.top - margin.bottom;

        gContainer
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        var x = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.count)])
            .range([0, gWidth]);

        var y = d3.scaleBand()
                .domain(data.map(d => d.count))
                .range([0, gHeight])
                .padding(0.1);
        
        var scaleColor = d3.scaleOrdinal(d3.schemeCategory10);


        //Bars
        var elements = gContainer            
            .selectAll("rect")
            .data(data)
            .enter()
        elements
            .append("rect")
            .attr("x", 0)
            .attr("y", d => y(d.count))
            .attr("width", d=> x(d.count) )
            .attr("height", y.bandwidth())
            .attr("fill",  d=> scaleColor(d.gender) )
            .on('mouseover', function (d, i) { //--- Mouse over
                
                //Highlight
                d3.select(this)
                    .attr("fill", config.colors.highlight)

                //show tooltip
                showTooltip(d,i);
            })
            .on('mouseout', function (d, i) { //--- Mouse out
                //color back
                d3.select(this)
                    .attr("fill",  d=> scaleColor(d.gender) )

                //hide tooltip
                Tooltip.hideTooltip();
            });


            //--- XAxis
            elements
                .append("text")
                .attr("class", "Axis")
                .attr("x", -10)
                .attr("y", (d,i) => y(data[i].count) + margin.top + 18 )
                .attr("fill", "white")
                .attr("cursor", "default")
                .attr("text-anchor", "end")
                .text(d => d.range);


        //show tooltip
        function showTooltip(d,i){
            //show title
            Tooltip.showTooltip_andHeader(d.range)

            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.count * 100 / config.country.people2017, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])

        }
    }
    render() {
        if (this.state.loaded == false)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetSimple title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetSimple>
                </div> 
            )
        else {
            return (
                <div className={"NoPadding " + this.props.className}>
                    <WidgetSimple title={this.state.title} chart={this.state.chart}></WidgetSimple>
                </div>
            );
        }
    }
}

export default AgeRange;