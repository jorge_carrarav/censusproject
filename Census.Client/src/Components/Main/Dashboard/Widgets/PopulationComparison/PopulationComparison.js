import React, { Component } from 'react';
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n'
import './PopulationComparison.css'
import * as Helper from '../../../../Shared/Helper.js'
import * as Dictionary from '../../../../Shared/Dictionary.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import * as d3 from "d3";



class PopulationComparison extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("populationComparison"),
            subtitle:  getString(""),
            chart:(
                <svg id="svg_PopulationComparison" className="SVG"></svg>
            ),
            loaded: false
        }

    }
    componentDidMount  () { //OnLoad
        //this.callAPI();
    }
    callAPI(filters){
        
        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"                
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/mapPopulation/regions", options)
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    loaded: true
                });
                var regionsData = data;
                for(var i = 0 ; i < regionsData.length; i++){
                    regionsData[i].place = Dictionary.RegionsAndCommunes(regionsData[i].place)
                }
        
                this.buildChart(regionsData);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        //this.callAPI(filters);
    }
    
    populationData(data){
        this.setState(
            {
                loaded: true
            }
        );
        //Limit the amount of items to show
        var dataLimited = []
        for(var i = 0; i < 15 && i < data.length; i++)
        {
            dataLimited.push({
                place: data[i].place,
                count: data[i].count
            })            
        }

        this.buildChart(dataLimited)
    }
    
    buildChart(data){
        var svgWidth = document.getElementById('svg_PopulationComparison').clientWidth;
        var svgHeight = document.getElementById('svg_PopulationComparison').clientHeight;
        
        var svg = d3.select('#svg_PopulationComparison').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg.append('g')
        var gCircles = svg.append('g')
        //--------------- Must have

  
        var chartProps = {
            marginTop: 100,
            itemSeparation: 33,
            circleSize_division: 50000, //the lower the bigger,
            rMin: 2,
            rMax: 150
        }


        var minPeople = d3.min(data, d => d.count)
        var maxPeople = d3.max(data, d => d.count)


        var blueRange = ["#D2F2F7", "#6097EB", "#2A3F88"]
        var uniqueColor = ["#6097EB", "#6097EB", "#6097EB"]
        var colorRange = blueRange;
        var colorScale = d3.scaleLinear()
            .domain([minPeople, maxPeople])
            .range(colorRange)

        var rScale = d3.scaleLinear()
            .domain([minPeople, maxPeople])
            .range([chartProps.rMin, chartProps.rMax])

              

        gCircles = gContainer
            .selectAll("circle")
            .data(data)
            .enter()

        //drowing circles
        gCircles
            .append("circle")
            .attr("cx", 50)
            .attr("cy", (d,i) => i*chartProps.itemSeparation + chartProps.marginTop)
            .attr("r", d => rScale(d.count))
            .attr("fill", d => colorScale(d.count))
            .attr("opacity", 0.7)
            .on('mouseover', function (d, i) {
                //highlight
                d3.select(this)
                    .attr("r", d => rScale(d.count) + 3)
                    .attr("fill", config.colors.highlight)

                showTooltip(d, i);                    
                    
            })
            .on('mouseout', function (d) {
                //highlight
                d3.select(this)
                    .transition()
                    .duration(config.animation.highlight)
                    .attr("fill", d => colorScale(d.count))
                    .attr("r", d => rScale(d.count))
                //hide tooltip
                Tooltip.hideTooltip();
            });
        //--- XAxis - Reigons
        gCircles
            .append("text")
            .attr("class", "Axis")
            .attr("x", 80)
            .attr("y", (d,i) => i*chartProps.itemSeparation + chartProps.marginTop + 5 )
            .attr("cursor", "default")
            .attr("fill", "white")
            .attr("text-anchor", "init")
            .text(d => d.place.replace("Región del ", "")
                                .replace("Región de ", "")
                                .replace("Región ", "")
                                .replace("de Santiago", "")
                                .replace("Libertador Bernardo ", "")
                                .replace("del Gral.Ibáñez del Campo", "")
                                .replace("y Antártica Chilena", ""));
        //text 2
        gCircles
            .append("text")
            .attr("class", "Axis")
            .attr("x", 20)
            .attr("y", (d,i) => i*chartProps.itemSeparation + chartProps.marginTop + 5 )
            .attr("cursor", "default")
            .attr("fill", "white")
            .attr("text-anchor", "end")
            .text(d => Helper.thousandSeparator(d.count));


        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gContainer);
        gContainer.attr("transform", "translate(" + alignMiddle[0] + " " + alignMiddle[1] + ") ")

        
        //tooltip
        function showTooltip(d,i){
            //show title
            Tooltip.showTooltip_andHeader(d.place)

            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.count * 100 / config.country.people2017, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])
        }
    }
    render() {
        if (this.state.loaded == false)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetDouble>
                </div>  
            )
        else{
            return (
                <div className={"NoPadding  " + this.props.className}>
                    <WidgetDouble title={this.state.title} 
                                    subtitle={this.state.subtitle}
                                    chart={this.state.chart}></WidgetDouble>
                </div>

            );
        }
    }
}

export default PopulationComparison;