import React, { Component } from 'react';
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import { getString } from '../../../../../i18n/i18n'
import './HouseHeadRelationship.css'
import * as d3 from "d3";

class HouseHeadRelationship extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("househeadrelationship"),
            chart:(
                <svg id="svg_ethnics" className="SVG">
                    
                </svg>
            )

        }
        
    }
    componentDidMount  () { //OnLoad
        this.buildChart()
    }
    buildChart(){

            

    }
    render() {
        return (
            <div className={"NoPadding " + this.props.className}>
                <WidgetDouble title={this.state.title}
                                 chart={this.state.chart}></WidgetDouble>
            </div>
        );
    }
}

export default HouseHeadRelationship;