import React, { Component } from 'react';
import WidgetSimple from '../Shared/WidgetSimple/WidgetSimple';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n'
import './Ethnic.css'
import * as Helper from '../../../../Shared/Helper.js'
import * as Dictionary from '../../../../Shared/Dictionary.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import * as d3 from "d3";


class Ethnic extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("ethnicsOnly"),
            subtitle:  "",
            chart:(
                <svg id="svg_ethnics" className="SVG">
                </svg>
            ),
            loaded: false
        }
        
    }

    componentDidMount  () { //OnLoad
        this.callAPI();
    }
    callAPI(filters){
        
        const options = {
            method: "POST", 
            mode: "cors", 
            cache: "no-cache", 
            credentials: "same-origin", 
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow", 
            referrer: "no-referrer", 
            body: JSON.stringify(filters), 
        }
        fetch(config.serviceUrl + "/api/data/ethnics", options)
        .then(res => res.json())
        .then((data) => {

                var ethnicsData = data;
                for(var i = 0 ; i < ethnicsData.length; i++){
                    ethnicsData[i].ethnic = Dictionary.Ethnic(ethnicsData[i].ethnic)
                }
                
                this.setState({
                    loaded: true
                });

                this.buildChart(ethnicsData);
            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }
    filterButtonClick(filters){
        this.setState(
            {
                loaded: false
            }
        );

        this.callAPI(filters);        
    }
    

    buildChart(data){
        var svgWidth = document.getElementById('svg_ethnics').clientWidth;
        var svgHeight = document.getElementById('svg_ethnics').clientHeight;
        var svg = d3.select('#svg_ethnics').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)
        var gContainer = svg
                            .append('g')
                            .attr("id", "gContainer_age")
        
        var tooltip_fade = config.animation.tooltip.fade;
        var tooltip_movement = config.animation.tooltip.movement;
        //--------------- Must have
        
        var chartProps = {
            margin: {
                top: 5,
                left: 100,
                right: 200,
                bottom: 20
            },
            barSeparation: 40,
            barInitPos: 15,
            barHeight: 30,
        }

        var scaleSize = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.count)])
            .range([0, svgWidth - chartProps.margin.right - chartProps.barInitPos]);

        
        //Bars
        var elements = gContainer            
            .selectAll("rect")
            .data(data)
            .enter()
        elements
            .append("rect")
            .attr("x", chartProps.barInitPos)
            .attr("y", (d, i) => i * chartProps.barSeparation)
            .attr("width", d=> scaleSize(d.count) )
            .attr("height", chartProps.barHeight)
            .attr("fill", "steelblue")
            .on('mouseover', function (d, i) { //--- Mouse over
                
                //Highlight
                d3.select(this)
                    .attr("fill", config.colors.highlight)
               
                //show tooltip
                showTooltip(d,i);
            })
            .on('mouseout', function (d, i) { //--- Mouse out
                //color back
                d3.select(this)
                    .attr("fill", "steelblue")

                //hide tooltip
                Tooltip.hideTooltip();
            });


            //--- XAxis
            elements
                .append("text")
                .attr("class", "Axis")
                .attr("x", -20)
                .attr("y", (d,i) => i * chartProps.barSeparation + chartProps.barHeight - 10)
                .attr("fill", "white")
                .attr("cursor", "default")
                .attr("text-anchor", "end")
                .text(d => d.ethnic);


        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gContainer);
        gContainer.attr("transform", "translate(" + (alignMiddle[0]) + " " + (alignMiddle[1] + chartProps.margin.top) + ") ")
        
 
        // show tooltip
        function showTooltip(d,i){
            //show title
            Tooltip.showTooltip_andHeader(d.ethnic)

            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(d.count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])            
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(d.count * 100 / config.country.people2017, 2) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])
        }

    }
    render() {
        if (this.state.loaded == false)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetSimple title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetSimple>
                </div> 
            )
        else{
            return (
                <div className={"NoPadding  " + this.props.className}>
                    <WidgetSimple title={this.state.title} 
                                    subtitle={this.state.subtitle}
                                    chart={this.state.chart}></WidgetSimple>
                </div>
            );
        }
    }
}

export default Ethnic;