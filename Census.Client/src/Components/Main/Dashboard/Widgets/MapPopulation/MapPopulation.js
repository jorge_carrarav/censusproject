import React, { Component } from 'react';
import WidgetDouble from '../Shared/WidgetDouble/WidgetDouble';
import config from '../../../../../config.json'
import { getString } from '../../../../../i18n/i18n'
import './MapPopulation.css'
import * as Helper from '../../../../Shared/Helper.js'
import * as Dictionary from '../../../../Shared/Dictionary.js'
import * as Tooltip from '../../../../Shared/Tooltip.js'
import * as d3 from "d3";



class MapPopulation extends Component {
    constructor(props) {
        super(props)
        this.state = 
        {
            title: getString("population"),
            chart:(
                <svg id="svg_map" className="SVG"></svg>
            ),
            mapPopulation_data: null,
            topoJson_data: null,
            levelName: "regions"
        }

    }
    componentDidUpdate(){
        var mapPopulation_data = this.state.mapPopulation_data;
        var topoJson_data = this.state.topoJson_data;
        
        if(this.state.mapPopulation_data != null &&
            this.state.topoJson_data != null)
                this.buildMap(mapPopulation_data, topoJson_data, this.state.levelName);
    }

    componentDidMount  () { //OnLoad
        this.callAPI();
        this.callTopoJsonAPI();
    }
    getLevelName(filters){
        var levelName;
        if(Helper.isEmpty(filters))
            levelName = "regions";
        else {
            if(filters.regionId == "")
                levelName = "regions";
            else
                levelName = "communes";
        }
        return levelName;
    }
    callAPI(filters){

        var levelName = this.getLevelName(filters);
        this.setState({
            levelName: levelName
        });
        const options = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
            },
            redirect: "follow",
            referrer: "no-referrer",
            body: JSON.stringify(filters),
        }
        fetch(config.serviceUrl + "/api/data/mapPopulation/" + levelName, options)
        .then(res => res.json())
        .then((data) => {

                var mapPopulation = data;
                for(var i = 0 ; i < mapPopulation.length; i++){
                    mapPopulation[i].place = Dictionary.RegionsAndCommunes(mapPopulation[i].place)
                }
                
                this.setState({
                    mapPopulation_data: mapPopulation
                });

                //filterButtonClick(filters)
                //sending data to PopulationSize component
                this.props.populationData(mapPopulation); //call APIs
               

            },
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }

    callTopoJsonAPI(filters){
        var regionid;

        if(Helper.isEmpty(filters))
            regionid = "all";
        else{
            if(filters.regionId == "")
                regionid = "all";
            else
                regionid = filters.regionId;
        }
        
        fetch(config.serviceUrl + "/api/topoJson/" + regionid) // /api/map/regionId
        .then(res => res.json())
        .then((data) => {
                this.setState({
                    topoJson_data: data
                });
            },
            (error) => {
                this.setState({
                    error: error
                });
            }
        )
    }

    filterButtonClick(filters){
        this.setState(
            {
                mapPopulation_data: null,
                topoJson_data: null
            }
        );


        this.callAPI(filters);
        this.callTopoJsonAPI(filters);      
    }

    buildMap(mapPopulation, mapTopojsonData, mapLevel){ //mapLevel: regions || mapLevel: commune
        
        var svgWidth = document.getElementById('svg_map').clientWidth;
        var svgHeight = document.getElementById('svg_map').clientHeight;
        
        var svg = d3.select('#svg_map').attr("viewBox", "0 0 " + svgWidth + " " + svgHeight)

        var gMap_zoom = svg.append("g")
        var gMap_translate = gMap_zoom.append("g")
        var gMap_scale = gMap_translate.append("g")
        var gMap = gMap_scale.append("g")
        var gLegend = svg.append("g")


        svg
            .call(d3.zoom()
                .scaleExtent([0.2, 5])
                .on("zoom", () => {
                    gMap_zoom.attr("transform", d3.event.transform);
                }));

        
        var projection = d3.geoMercator()
            .scale(1000)            
        var path = d3.geoPath()
            .projection(projection)
       
        var minPeople = d3.min(mapPopulation, d => d.count)
        var maxPeople = d3.max(mapPopulation, d => d.count)

        var greenRange = ["#AED4B0", "#4D8B4F", "#194108"]
        var blueRange = ["#D2F2F7", "#6097EB", "#2A3F88"]

        var colorRange = blueRange;
        var avgPeople = (maxPeople + minPeople) / 5;
        var colorScale = d3.scaleLinear()
                        .domain([minPeople, avgPeople, maxPeople])
                        .range(blueRange)
        
/*
        var yellowRed_arr = ["white", "#EFEF8E", "#E3AF24", "#EB530E", "#DC0100", "#8C0C1B"]
        
        var people_arr = [];
        for(var i = 0; i< yellowRed_arr.length;i++){
            var num = i * (maxPeople / (yellowRed_arr.length - 1))
            num = parseInt(num)
            people_arr.push(num)
        }
        var colorScale = d3.scaleLinear()
            .domain(people_arr)
            .range(yellowRed_arr)
*/


        function getPlace_by_name(name){
            var index = mapPopulation.findIndex(obj => obj.place == name)
            return mapPopulation[index];
        }


        //Drawing the map
        gMap
            .selectAll("path")
            .data(mapTopojsonData)
            .enter()
            .append("path")
            .attr("d", path)
            .attr("fill", (d, i) => {
                var placeName = mapLevel == "regions"? d.properties.nom_reg: d.properties.nom_com;                

                if(placeName != ""){
                    if(getPlace_by_name(placeName) === undefined)
                        return colorScale(minPeople);
                    else
                        return colorScale(getPlace_by_name(placeName).count)
                }
            })
            .on("click", d => {
                console.log(d)
                console.log(d)
            })
            .on('mouseover', function (d, i) {
                d3.select(this)
                    .attr("fill", config.colors.highlight)
                showTooltip(d, i, minPeople);
            })
            .on('mouseout', function (d, i) {

                d3.select(this)
                    .attr("fill", (d) => {
                        var placeName = mapLevel == "regions"? d.properties.nom_reg: d.properties.nom_com;
                        if(placeName != ""){
                            if(getPlace_by_name(placeName) === undefined)
                                return colorScale(minPeople);
                            else
                                return colorScale(getPlace_by_name(placeName).count)
                        }
                    })
               
                //hide tooltip
                Tooltip.hideTooltip();
            });
        
        
        //First move the map to 0, 0
        var posX = svg.node().getBoundingClientRect().left - gMap.node().getBoundingClientRect().left;
        var posY = svg.node().getBoundingClientRect().top - gMap.node().getBoundingClientRect().top;
        gMap.attr("transform", "translate(" + posX + " " + posY + ") ")

        //Second scale it
        var margin = 30;   
        var percReduction = this.calcPercentageOfScale(svg, gMap, margin)
        gMap_scale.attr("transform", "scale(" + percReduction + ")")

        //Final translation - Align to the center        
        var alignMiddle = Helper.alignMiddle_svg_g(svg, gMap);
        gMap_translate.attr("transform", "translate(" + alignMiddle[0] + " " + alignMiddle[1] + ") ")


        //Legend
        var margin_legend = {
            left: 10,
            top: 15,
            textLeft: 20,
            textTop: 25       
        }
        gLegend
            .append("rect")
            .attr("x", margin_legend.left)
            .attr("y", margin_legend.top)
            .attr("width", 180)
            .attr("height", 60)
            .attr("fill", "white")
            .attr("rx", 15)
            .attr("opacity", 0.1)
        gLegend
            .append("rect")
            .attr("x", margin_legend.textLeft)
            .attr("y", margin_legend.textTop)
            .attr("width", 20)
            .attr("height", 10)
            .attr("fill", colorRange[2])
            .attr("rx", 5)
        gLegend
            .append("rect")
            .attr("x", margin_legend.textLeft)
            .attr("y", margin_legend.textTop + 25)
            .attr("width", 20)
            .attr("height", 10)
            .attr("fill", colorRange[0])
            .attr("rx", 5)
        gLegend
            .append("text")
            .attr("x", margin_legend.textLeft + 35)
            .attr("y", margin_legend.textTop + 10)
            .attr("cursor", "default")
            .attr("fill", "white")
            .attr("height", 10)
            .text(getString("morePeople"))
        gLegend
            .append("text")
            .attr("x", margin_legend.textLeft + 35)
            .attr("y", margin_legend.textTop + 35)
            .attr("cursor", "default")
            .attr("cursor", "default")
            .attr("fill", "white")
            .attr("height", 10)
            .text(getString("lessPeople"))
        
            
        //tooltip
        function showTooltip(d, i, minPeople){
            var placeName = mapLevel == "regions"? d.properties.nom_reg: d.properties.nom_com;

            var count;
            if(getPlace_by_name(placeName) === undefined)
                count = 0;
            else
                count = getPlace_by_name(placeName).count;


            //show title
            Tooltip.showTooltip_andHeader(placeName)

            //text
            Tooltip.addNewLine([
                {"text":Helper.thousandSeparator(count), "fontWeight": "bold"},
                {"text":getString("people").toLowerCase(), "fontWeight": "normal"}                
            ])   

            var decimals = mapLevel == "regions"? 2: 2;
            Tooltip.addNewLine([
                {"text":Helper.limitDecimals(count * 100 / config.country.people2017, decimals) + "%", "fontWeight": "bold"},
                {"text":getString("ofTheCountry"), "fontWeight": "normal"}                
            ])
        }
    }
    calcPercentageOfScale(svg, gMap, margin){
        // calculating the size of the SVG and the G (map created) 
        /*
            Applying 2 reductions
            - The first got from the Height, then applied over the width to see the result
            - If the result still bigger than SVG + Margnin, then calculate a 2nd reduction
            With those 2 reductions, get the pixels of the final width. 
            The difference between current gWidth and the final width (with 2 reductions applied) are
            the pixels of reduction.
            Finally calculate which % represent those pixels
        */
        var svgWidth = svg.node().getBoundingClientRect().width;
        var svgHeight = svg.node().getBoundingClientRect().height;
        var gWidth = gMap.node().getBoundingClientRect().width;
        var gHeight = gMap.node().getBoundingClientRect().height;

        var percReduction  = (svgHeight - margin*2) / gHeight;

        //Width result after reduction 1
        var width_afterReduction_1 = gWidth * percReduction;
        var percReduction_2;
        if((svgWidth - margin*2) < width_afterReduction_1){ //Checkif needed a 2nd reduction
            percReduction_2 = (svgWidth - margin*2) / width_afterReduction_1;
            //Width result after reduction 2
            var width_afterReduction_2 = width_afterReduction_1 * percReduction_2; 
            percReduction = width_afterReduction_2 / gWidth;
        }
        
        return percReduction;
    }
     
    render() {
        if (this.state.mapPopulation_data == null || 
            this.state.topoJson_data == null)
            return (               
                <div className={"NoPadding " + this.props.className}>
                    <WidgetDouble title={this.state.title} 
                                    chart={(
                                        <div className="Loading"></div>
                                    )}></WidgetDouble>
                </div>  
            )
        else{
            return (
                <div className={"NoPadding  " + this.props.className}>
                    <WidgetDouble title={this.state.title} chart={this.state.chart}></WidgetDouble>
                </div>

            );
        }
    }
}

export default MapPopulation;