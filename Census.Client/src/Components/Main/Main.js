import React, { Component } from 'react';
import "./Main.css"
import Dashboard from './Dashboard/Dashboard';


class Main extends Component {
    constructor(props) {
      super(props)
      
      this.dashboardComponent = React.createRef();
    }
    filterButtonClick = (filters) => {
      this.dashboardComponent.current.filterButtonClick(filters);
    }

   
    render() {
        return (
            <div className="Main">
                    <div className="HeaderContainer">
                    </div>
                <Dashboard ref={this.dashboardComponent}></Dashboard>
            </div>
        );
    }
}

export default Main;