import React, { Component, useRef } from 'react';
import './App.css';
import Main from '../Main/Main';
import LeftPanel from '../LeftPanel/LeftPanel';
import '../Shared/Styles/ScrollBar.css';
import '../Shared/Styles/Common.css';


class App extends Component {
  constructor(props) {
    super(props)
    
    this.mainComponent = React.createRef();
  }
  filterButtonClick = (filters) => {
    this.mainComponent.current.filterButtonClick(filters);
  }
  render() {
    return (
      <div className="App ">
      
        <div className="BackgroundImage"></div>
        <div className="BackgroundImage_layer2"></div>
        <div className="BackgroundImage_layer3"></div>
        
        <div className="container-fluid NoPadding">
          <div className="AppContainer">
            <div id="AppLeftContainer" className="AppLeftContainer_Filter_opened">
              <LeftPanel filterButtonClick={this.filterButtonClick}></LeftPanel>
            </div>
            <div id="AppRightContainer" className="AppRightContainer_Filter_opened">
              <Main ref={this.mainComponent}></Main>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
