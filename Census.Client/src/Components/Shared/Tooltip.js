import config from '../../config.json'
import './Styles/Tooltip.css'
import * as d3 from "d3";

export function hideTooltip(){
    d3.select("#d3tooltipContainer")
        .transition()
        .duration(config.animation.tooltip.fade)
        .style("opacity", 0)
}
export function showTooltip_andHeader(titleText){
    var tooltip_fade = config.animation.tooltip.fade;
    var tooltip_movement = config.animation.tooltip.movement;

    //Tooltip - show
    var tooltip_xPos = d3.event.pageX + document.getElementById('d3tooltip').clientWidth + 10 > document.body.clientWidth ? d3.event.pageX - document.getElementById('d3tooltip').clientWidth - 10 : d3.event.pageX;
    var tooltip_yPos = d3.event.pageY;
    
    
    d3.select("#d3tooltipContainer")
        .transition()
        .duration(tooltip_movement)
        .style("left", tooltip_xPos + "px")
        .style("top", tooltip_yPos + "px")
        .transition()
        .duration(tooltip_fade)
        .style("opacity", 1)
        .style("fill", "white")



    d3.select("#d3tooltip").html('')//clean text
    if(titleText !== undefined){
    //--- Tooltip - Header
        d3.select("#d3tooltip")
        .append("text")
        .style("font-weight", "bold")
        .text(titleText)
    }
}
export function addNewLine(lines){ //lines = array

    d3.select("#d3tooltip").append("br") // new line -----------
    if(lines !== undefined)
        lines.forEach(line => {
            d3.select("#d3tooltip")
            .append("text")
            .style("font-weight", line.fontWeight)
            .text(line.text + " ")
        });

}