import config from '../../config.json'

export function thousandSeparator(num){
    let result = num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return config.formats.thousandSeparator === '.'?  result.replace(",",".").replace(",",".") : result.replace(".",",").replace(".",",");
}

export function limitDecimals(num, decimals){
    return parseFloat(Math.round(num * 100) / 100).toFixed(decimals);
}


export function alignMiddle_svg_g(svg, g){
    var svgLeft = svg.node().getBoundingClientRect().left;
    var svgRight = svg.node().getBoundingClientRect().right;

    var g_left = g.node().getBoundingClientRect().left;
    var g_right = g.node().getBoundingClientRect().right;

    var x = (svgLeft - g_left + svgRight - g_right) / 2;

    var y = (svg.node().getBoundingClientRect().height - g.node().getBoundingClientRect().height) / 2;

    return [x,y]
}


export function isEmpty(obj){
    for(var key in obj)
        if(obj.hasOwnProperty(key))
            return false;

    return true;
}