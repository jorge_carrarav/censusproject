import Dictionary_regionsAndCommunes from '../../Dictionary/regionsAndCommunes.json'
import Dictionary_industry from '../../Dictionary/industry_en.json'
import Dictionary_countries from '../../Dictionary/countries_en.json'
import Dictionary_ethnic from '../../Dictionary/ethnics.json'
import Dictionary_academicLevel from '../../Dictionary/academicLevel.json'

export function RegionsAndCommunes(id){
    var index = Dictionary_regionsAndCommunes.findIndex(obj => obj.id == id)
    return Dictionary_regionsAndCommunes[index].value;   
}

export function RegionsAndCommunesId_byValue(value){
    var index = Dictionary_regionsAndCommunes.findIndex(obj => obj.value == value)
    return Dictionary_regionsAndCommunes[index].id;   
}

export function Country(id){
    var index = Dictionary_countries.findIndex(obj => obj.id == id)
    return Dictionary_countries[index].value;  
}
export function CountryId_byValue(value){
    var index = Dictionary_countries.findIndex(obj => obj.value == value)
    return Dictionary_countries[index].id;  
}

export function Ethnic(id){
    var index = Dictionary_ethnic.findIndex(obj => obj.id == id)
    return Dictionary_ethnic[index].value;  
}

export function EthnicId_byValue(value){
    var index = Dictionary_ethnic.findIndex(obj => obj.value == value)
    return Dictionary_ethnic[index].id;  
}

export function AcademicLevel(id){
    console.log(id)
    var index = Dictionary_academicLevel.findIndex(obj => obj.id == id)
    return Dictionary_academicLevel[index].value;  
}


export function Industry(id){
    var index = Dictionary_industry.findIndex(obj => obj.id == id)
    return Dictionary_industry[index];  
}