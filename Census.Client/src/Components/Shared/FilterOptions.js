import Dictionary_regions from '../../Dictionary/regions.json'
import Dictionary_countries from '../../Dictionary/countries_en.json'
import Dictionary_ethnic from '../../Dictionary/ethnics.json'

export function Regions(){
    var regions_arr = [];
    for(var i=0; i<Dictionary_regions.length;i++){
        regions_arr.push(Dictionary_regions[i].value)
    }

    return regions_arr;
}
export function Countries(){
    var countries_arr = [];
    for(var i=0; i<Dictionary_countries.length;i++){
        countries_arr.push(Dictionary_countries[i].value)
    }

    return countries_arr;   
}
export function Ethnics(){
    var ethnics_arr = [];
    for(var i=0; i<Dictionary_ethnic.length;i++){
        ethnics_arr.push(Dictionary_ethnic[i].value)
    }

    return ethnics_arr;
}