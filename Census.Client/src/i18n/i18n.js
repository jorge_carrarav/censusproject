import config from '../config.json'
import es from './es.json'
import en from './en.json'

let language;

function setLanguage(){
    language = config.lang === 'en'? en : es;
}
setLanguage();

export function getString(key){
    return language[key];
}